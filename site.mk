# Versionstring
DEFAULT_GLUON_RELEASE := 0.10.0-beta1-$(shell date '+%Y%m%d')

GLUON_RELEASE ?= $(DEFAULT_GLUON_RELEASE)

# Default priority for updates
GLUON_PRIORITY ?= 7

# Languages to include
GLUON_LANGS ?= en de

# Region code required for some images; supported values: us eu
GLUON_REGION = eu

# Prefer ath10k firmware with 802.11s support
GLUON_WLAN_MESH = 11s

# Build gluon with multidomain support
GLUON_MULTIDOMAIN = 1

# TODO: research on every module and write documentation
