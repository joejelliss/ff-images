features ({
	'alfred',
	'autoupdater',
        'config-mode-core',
        'config-mode-domain-select',
        'config-mode-geo-location-osm',
        'ebtables-filter-multicast',
        'ebtables-filter-ra-dhcp',
        'mesh-batman-adv-15',
	'mesh-vpn-fastd',
	'mesh-vpn-fastd-l2tp',
        'radvd',
        'radv-filterd',
        'respondd',
        'setup-mode',
        'ssid-changer',
        'status-page',
        'web-advanced',
	'web-mesh-vpn-fastd',
        'web-private-wifi',
        'web-wizard'

})

packages ({
	'gluon-ssid-changer',
	'haveged',
	'iwinfo'
})

if target ('x86-64') then
	packages {
		'qemu-ga',
		'open-vm-tools',
		'kmod-usb-hid'
	}
end

if target('x86', '64') then
    -- add guest agent for qemu and vmware
    packages {
        'qemu-ga',
        'open-vm-tools',
    }
end

if target('x86-generic') then
	packages {
		'kmod-usb-hid'
	}	
end

if not device_class('tiny') and not target('ramips', 'mt7620') then
    features {
        'tls',
        'wireless-encryption-wpa3',
    }
    packages {
        'openssh-sftp-server',
    }
end

if target('ramips', 'mt76x8') and not device({
    'gl-mt300n-v2',
    'gl.inet-microuter-n300',
    'netgear-r6120',
    'ravpower-rp-wd009',
}) then
    include_usb = false
end

